from django.shortcuts import render


def index(request):
    template_name = 'inicio.html'
    return render(request, template_name)

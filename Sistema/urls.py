from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from rest_framework import routers
from django.conf import settings
from django.conf.urls import static
from Cadastros.api.viewsets import UsuariosViewSet
from consultas.api.viewsets import ConsultaUsuariosViewSet
from editar.api.viewsets import ModificarrUsuariosViewSet

router = routers.DefaultRouter()
router.register(r'Usuarios', UsuariosViewSet, base_name='cadastros')
router.register(r'Consultas', ConsultaUsuariosViewSet, base_name='Usuarios')
router.register(r'Editar', ModificarrUsuariosViewSet, base_name='editar')

urlpatterns = [
    #path('', include('inicio.urls')),
    path('', include(router.urls)),
    path('admin/', admin.site.urls),
]# + static(settings.MEDIA_URL, document_rot=settings.MEDIA_ROOT)

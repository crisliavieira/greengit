from rest_framework.viewsets import ModelViewSet
from Cadastros.models import Usuarios
from .serializers import ConsultaUsuarioSerializer


class ConsultaUsuariosViewSet(ModelViewSet):
    serializer_class = ConsultaUsuarioSerializer

    def get_queryset(self):
        nome = self.request.query_params.get('nome', None)
        sobrenome = self.request.query_params.get('sobrenomenome', None)
        estado = self.request.query_params.get('estado', None)
        cidadde = self.request.query_params.get('cidade', None)
        endereco = self.request.query_params.get('endereco', None)
        email = self.request.query_params.get('email', None)
        queryset = Usuarios.objects.all()
        if nome:
            queryset = queryset.filter(nome__icontains=nome)
        if sobrenome:
            queryset = queryset.filter(sobrenome__icontains=sobrenome)
        if estado:
            queryset = queryset.filter(estado__icontains=estado)
        if cidadde:
            queryset = queryset.filter(cidadde__icontains=cidadde)
        if endereco:
            queryset = queryset.filter(endereco__icontains=endereco)
        if email:
            queryset = queryset.filter(email__icontains=email)

        return queryset

    def list(self, request, *args, **kwargs):
        return super(ConsultaUsuariosViewSet, self).list(self, *args, **kwargs)

    # def create(self, request, *args, **kwargs):
    #     return super(ConsultaUsuariosViewSet, self).create(self, *args, **kwargs)
    #
    # def destroy(self, request, *args, **kwargs):
    #     return super(ConsultaUsuariosViewSet, self).destroy(self, *args, **kwargs)
    #
    # def retrieve(self, request, *args, **kwargs):
    #     return super(ConsultaUsuariosViewSet, self).retrieve(self, *args, **kwargs)
    #
    # def update(self, request, *args, **kwargs):
    #     return super(ConsultaUsuariosViewSet, self).update(self, *args, **kwargs)

    # def partial_update(self, request, *args, **kwargs):
    #     pass
    #
    # @action(methods=['get'], id=True)
    # def personalizada(self, request, pk=None):
    #     pass
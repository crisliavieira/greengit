from rest_framework.serializers import ModelSerializer
from Cadastros.models import Usuarios


class ConsultaUsuarioSerializer(ModelSerializer):

    class Meta:
        model = Usuarios
        fields = ('nome', 'sobrenome', 'estado', 'cidade', 'endereco', 'email', )

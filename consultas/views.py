from django.shortcuts import render, get_object_or_404, redirect
from django.http import Http404
from django.core.paginator import Paginator
# from .models import Users
from django.db.models import Q, Value
from django.contrib import messages


def consulta(request):
    template_name = 'tela-de-consulta.html',
    # user = Users.objects.all()
    return render(request, template_name, {'user': user})

from rest_framework.viewsets import ModelViewSet
from Cadastros.models import Usuarios
from .serializers import ModificarUsuarioSerializer


class ModificarrUsuariosViewSet(ModelViewSet):
    serializer_class = ModificarUsuarioSerializer
    queryset = Usuarios.objects.all()
    filter_fields = ('nome', 'email', 'cidade')
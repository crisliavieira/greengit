from django.shortcuts import render, get_object_or_404, redirect
from django.http import Http404
from django.core.paginator import Paginator
# from .models import Users
from django.db.models import Q, Value
from django.contrib import messages


def editar(request):
    template_name = 'editar-usuario.html'
    return render(request, template_name)
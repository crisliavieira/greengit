from rest_framework.filters import SearchFilter
from rest_framework.viewsets import ModelViewSet
from Cadastros.models import Usuarios
from .serializers import UsuariosSerializer


class UsuariosViewSet(ModelViewSet):
    serializer_class = UsuariosSerializer
    queryset = Usuarios.objects.all()
    filter_backends = (SearchFilter,)
    search_fields = ('nome', 'email', 'cidade')
from rest_framework.serializers import ModelSerializer
from Cadastros.models import Usuarios


class UsuariosSerializer(ModelSerializer):

    class Meta:
        model = Usuarios
        fields = ('nome', 'sobrenome', 'endereco', 'email',)

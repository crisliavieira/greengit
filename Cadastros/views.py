from django.shortcuts import render


def cadastros(request):
    template_name = 'tela-de-cadastro.html'
    return render(request, template_name)
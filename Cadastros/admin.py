from django.contrib import admin
from .models import Usuarios


class UserAdmin(admin.ModelAdmin):
    list_display = ['nome', 'sobrenome', 'estado', 'cidade', 'endereco', 'email', 'senha', 'confirmasenha']
    search_fields = ['nome', 'sobrenome', 'estado', 'cidade', 'endereço', 'eail', 'senha', 'confirmasenha']


admin.site.register(Usuarios, UserAdmin)
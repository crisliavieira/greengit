from django.db import models


class Usuarios(models.Model):
    nome = models.CharField(max_length=255, blank=False)
    sobrenome = models.CharField(max_length=255, blank=False)
    estado = models.CharField(max_length=255, blank=False)
    cidade = models.CharField(max_length=255, blank=False)
    endereco = models.CharField(max_length=255, blank=False)
    email = models.EmailField(max_length=50, blank=False)
    senha = models.CharField(max_length=255, blank=False)#forms.PasswordImput
    confirmasenha = models.CharField(max_length=255, blank=False)
    # foto = models.ImageField(upload_to='fotos_perfil', null=True, blank=True)

    def __str__(self):
        return self.nome

    class Meta:
        db_table = 'Usuários'
        verbose_name = 'usuário'
        verbose_name_plural = 'usuários'


